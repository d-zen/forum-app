import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'sanitizer'
})
export class SanitizerPipe implements PipeTransform {

  constructor(
    private sanitizer: DomSanitizer
  ) {}

  // tslint:disable-next-line:typedef
  transform(value: any) {
    return this.sanitizer.bypassSecurityTrustHtml(value);
  }
}
