import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PostsService } from '../../services/posts.service';
import { Post } from '../../models';

@Component({
  selector: 'app-home',
  templateUrl: './posts.component.html',
  styleUrls: [ './posts.component.scss' ]
})
export class PostsComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  posts: Post[];
  loader: boolean;

  constructor(
    private postsService: PostsService,
    protected router: Router
  ) {
  }

  ngOnInit(): void {
    this.getPosts();
  }


  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }


  getPosts(): void {
    this.loader = true;
    this.postsService.getPosts().subscribe(resp => {
        this.loader = false;
        this.posts = resp;
      }, err => {
        // console.error(err);
        this.loader = false;
      }
    );
  }

  navigate(id: number | string): void {
    const url = `post/${ id }`;
    this.router.navigateByUrl(url);
  }
}
