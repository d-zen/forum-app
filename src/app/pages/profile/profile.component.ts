import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public userName: string;

  constructor(
    public authService: AuthService
  ) { }

  ngOnInit(): void {
    this.userName = this.authService.getLocalUser();
  }

  setUserName(form: any): void {
    if (form.valid) {
      this.authService.setLocalUserName(form.value.userName);
    }
  }
}
