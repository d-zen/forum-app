import { Component, Input, OnInit } from '@angular/core';
import { Post } from '../../models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-preview',
  templateUrl: './post-preview.component.html',
  styleUrls: ['./post-preview.component.scss']
})
export class PostPreviewComponent implements OnInit {
  @Input() post: Post;

  constructor(
   protected router: Router
  ) { }

  ngOnInit(): void {
  }

  navigate(id: string): void {
    const url = `posts/${id}`;
    this.router.navigateByUrl(url);
  }

  isHTML(str: string): boolean {
    return /<\/?[a-z][\s\S]*>/i.test(str);
  }
}
