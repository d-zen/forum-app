import { Component, OnInit } from '@angular/core';
import { Post } from '../../models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PostsService } from '../../services/posts.service';
import { AuthService } from '../../auth/auth.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Utils from '../../utils/utils';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: [ './post-form.component.scss' ]
})
export class PostFormComponent implements OnInit {
  public Editor = ClassicEditor;
  public post: Post;
  public id: string;
  public disabled: boolean;
  public showToast: boolean;

  postForm: FormGroup = this.formBuilder.group({
    title: [ null, Validators.compose([ Validators.required ]) ],
    body: [ null, Validators.compose([ Validators.required ]) ],
    author: [localStorage.getItem('user')],
    postedAt: [new Date()],
    img: ['https://i.picsum.photos/id/445/200/200.jpg?hmac=IJGybzd6hRYuiwyBiBXZ_3cOjM0MrrTpARBSFzypGNI'],
    id: [ Utils.randomID ],
    comments: [[]]
  });

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private postsService: PostsService,
    private formBuilder: FormBuilder,
  ) {
  }

  ngOnInit(): void {
  }

  submitPost(): void {
    if (this.postForm.valid) {
      this.disabled = true;
      this.postsService.submitNewPost(this.postForm.value).subscribe(post => {
        this.disabled = false;
        this.showToast = true;
        this.postForm.reset();
        setTimeout( _ => {
          this.showToast = false;
        }, 2000);
      }, err => {
        this.disabled = false;
      });
    }
  }
}
