import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostsService } from '../../services/posts.service';
import { Post } from '../../models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Utils from '../../utils/utils';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})

export class PostComponent implements OnInit {
  public post: Post;
  public id: string;
  public loading: boolean;
  public disabled: boolean;

  commentForm: FormGroup = this.formBuilder.group({
    body: [ '', Validators.compose([ Validators.required ]) ],
  });


  constructor(
    private route: ActivatedRoute,
    private postsService: PostsService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.getPost();
  }

  private getPost(): void {
    this.loading = true;
    this.id = this.route.snapshot.paramMap.get('id');
    this.postsService.getPost(this.id).subscribe(post => {
      this.post = post;
      this.loading = false;
    }, error => this.loading = false);
  }

  submitComment(): any {
    if (this.commentForm.valid) {
      this.disabled = true;
      const newComment = this.commentForm.value;
      newComment.commentedAt = new Date();
      newComment.author = localStorage.getItem('user');
      this.postsService.submitComment(this.post, newComment).subscribe((post: Post) => {
        this.post.comments = post.comments;
        this.disabled = false;
        this.commentForm.reset();
      }, err => {
        this.disabled = false;
      });
    }
  }
}
