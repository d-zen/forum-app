export class Post {
  id: string;
  img: string;
  title: string;
  postedAt: Date;
  body: string;
  author: string;
  comments: Comment[];
}

export class Comment {
  id: string;
  author: string;
  body: string;
  commentedAt: Date;
}
