import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Credentials, User } from './model';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  public user: User;
  protected baseUrl = 'api/login';

  constructor(
    private http: HttpClient,
    private router: Router,
  ) {
  }

  isAuthenticated(): boolean {
    return !!localStorage.getItem('user');
  }

  login(credentials: Credentials): Observable<boolean> {
    return this.http.post(this.baseUrl, credentials).pipe(
      map((user: User) => {
        if (user) {
          this.setLocalUserName(user.name);
          return this.isAuthenticated();
        }
      }, err => {
        return this.isAuthenticated();
      }),
      catchError(this.handleError<User>('getUser', null)));
  }

  logOut(): Observable<boolean> {
    this.clearLocalUser();
    return of(this.isAuthenticated());
  }

  public getLocalUser(): string {
    return localStorage.getItem('user');
  }

  public clearLocalUser(): void {
    localStorage.setItem('user', '');
  }

  public setLocalUserName(name: string): void {
    localStorage.setItem('user', name);
  }

  redirectToLogin(): void {
    this.router.navigate([ '/login' ], { replaceUrl: true });
  }

  private handleError<T>(operation = 'operation', result?: T): any {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

}
