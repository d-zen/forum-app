export class User {
  id: number | string;
  name: string;
  login: string;
  pass: string;

  constructor(name: string) {
    this.name = name;
  }
}

export class Credentials {
  login: string;
  password: string;

  constructor(login, pass) {
    this.login = login;
    this.password = pass;
  }
}
