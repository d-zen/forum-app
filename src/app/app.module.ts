import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopbarComponent } from './layout/topbar/topbar.component';
import { LoginComponent } from './auth/components/login/login.component';
import { PostsComponent } from './pages/posts/posts.component';
import { AuthService } from './auth/auth.service';
import { DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from './auth/auth.guard';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './services/in-memory-data.service';
import { FooterComponent } from './layout/footer/footer.component';
import { PostPreviewComponent } from './pages/post-preview/post-preview.component';
import { PostComponent } from './pages/post/post.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PostFormComponent } from './pages/post-form/post-form.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { SanitizerPipe } from './pipes/sanitizer.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    LoginComponent,
    PostsComponent,
    FooterComponent,
    PostComponent,
    PostPreviewComponent,
    NotFoundComponent,
    PostFormComponent,
    ProfileComponent,
    SanitizerPipe,
  ],

  imports: [
    BrowserModule,
    CKEditorModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    FormsModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    DecimalPipe,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}
