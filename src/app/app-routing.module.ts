import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/components/login/login.component';
import { AuthGuard } from './auth/auth.guard';
import { PostsComponent } from './pages/posts/posts.component';
import { PostComponent } from './pages/post/post.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PostFormComponent } from './pages/post-form/post-form.component';
import { ProfileComponent } from './pages/profile/profile.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'prefix',
    children: [
      {
        path: '', redirectTo: 'posts', pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'posts',
        component: PostsComponent,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'posts/:id',
        component: PostComponent,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'post-form',
        component: PostFormComponent,
        canActivate: [ AuthGuard ]
      },
      {
        path: '**',
        component: NotFoundComponent,
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
}
