import { Injectable } from '@angular/core';
import {InMemoryDbService, ResponseOptions, STATUS} from 'angular-in-memory-web-api';
import { USERS } from '../users-mock';
import { POSTS } from '../posts-mock';
import Utils from '../utils/utils';

@Injectable({
  providedIn: 'root'
})

export class InMemoryDataService implements InMemoryDbService {
  protected loginUrl = 'api/login';

  createDb(): any {
    return {
      users: USERS,
      posts: POSTS
    };
  }

  put(requestInfo: RequestInfo, db: {}): any {
    const reqBody = requestInfo['req']['body'];
    const options: ResponseOptions = {
      body: null,
      status: null,
      headers: requestInfo['headers'],
      url: requestInfo['url']
    };
    const existingPost = POSTS.find(post => post.id === requestInfo['id']);

    if (existingPost) {
      const newComment = reqBody;
      newComment.id = Utils.randomID;
      existingPost.comments.push(newComment);
      options.body = existingPost;
      options.status = STATUS.OK;
    } else {
      options.status = STATUS.INTERNAL_SERVER_ERROR;
    }
    return requestInfo['utils'].createResponse$(() => options);
  }

  post(requestInfo: RequestInfo, db: {}): any {
    if (requestInfo['url'] === this.loginUrl) {
      const reqBody = requestInfo['req']['body'];
      const options: ResponseOptions = {
        body: null,
        status: null,
        headers: requestInfo['headers'],
        url: requestInfo['url']
      };

      const existingUser = USERS.find(user => user.name === reqBody.login);

      if (existingUser) {
        if (reqBody.password === existingUser.pass) {
          options.body = existingUser;
          options.status = STATUS.OK;
        }
      } else {
        options.status = STATUS.UNAUTHORIZED;
      }
      return requestInfo['utils'].createResponse$(() => options);
    }
  }

}
