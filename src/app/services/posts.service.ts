import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Post, Comment } from '../models';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class PostsService {
  protected baseUrl = 'api/posts';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
  ) {
  }

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.baseUrl);
  }

  getPost(id: number | string): Observable<Post> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.get<Post>(url);
  }

  submitNewPost(value: any): Observable<Post> {
    const url = `${this.baseUrl}`;
    return this.http.post(url, value, this.httpOptions).pipe(
      tap((newPost: any) => console.log(`added post w/ id=${newPost.id}`)),
      catchError(this.handleError<Comment>('newPost'))
    );
  }

  submitComment(post: Post, comment: Comment): Observable<any> {
    return this.http.put(`${this.baseUrl}/${post.id}`, comment, this.httpOptions).pipe(
      tap(_ => console.log(`added comment id=${post.id}`)),
      catchError(this.handleError<any>('addComment'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T): any {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
