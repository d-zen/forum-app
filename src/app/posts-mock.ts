import { Post } from './models';
import Utils from './utils/utils';

export const POSTS: Post[] = [
  {
    id: Utils.randomID,
    title: 'Heading 1',
    img: 'https://i.picsum.photos/id/490/200/200.jpg?hmac=7WZhaN9NS8sb08YmpHre_3NGnVUsmH8X5W_GlG2Mry4',
    postedAt: new Date(),
    body: 'A alias aliquid architecto at aut beatae blanditiis corporis culpa dignissimos eligendi error harum illo, incidunt itaque iusto laborum mollitia nihil numquam, odio optio pariatur quae recusandae reiciendis repellendus reprehenderit sed sequi sunt tempora ullam unde, ut vel vero voluptatibus. A adipisci aliquam blanditiis cum deleniti eveniet explicabo harum hic incidunt laudantium magni minima modi neque, nihil obcaecati officiis qui quisquam ratione sapiente sint soluta tempora vero? At beatae deleniti earum error et excepturi expedita facere fugiat hic illo incidunt laboriosam maiores minima, molestiae molestias nemo nihil nostrum nulla officia perferendis placeat quia quisquam rem repudiandae totam vel vero vitae. Ab, ad blanditiis cupiditate est exercitationem facilis illo laboriosam maiores odit, officiis praesentium, quaerat qui rem sed tempore. Aliquam architecto corporis dignissimos dolor, dolores eius error eveniet facere fugit magnam, porro quidem sed, sint soluta?',
    author: 'someBody',
    comments: [
      {
        id: Utils.randomID,
        author: 'Julius',
        body: 'Dictum! Factum!',
        commentedAt: new Date()
      },
      {
        id: Utils.randomID,
        author: 'Cicero',
        body: 'Veni! Vidi! Vici!',
        commentedAt: new Date()
      },
    ]
  },
  {
    id: Utils.randomID,
    title: 'Heading 2',
    img: 'https://i.picsum.photos/id/551/200/200.jpg?hmac=vxRitsvMJEbK15DKl4ZRj7NQWF6RTfzBvievdi9q96s',
    postedAt: new Date(),
    body: 'Autem commodi cum cumque distinctio dolor excepturi magnam quo reprehenderit vero vitae. Autem dolor eius enim ex ipsa labore laudantium maiores pariatur sed tenetur! Consectetur culpa fugit nobis non quidem? Alias delectus distinctio dolor eaque ex excepturi fugiat nobis omnis quo quos! Adipisci animi dignissimos distinctio? Aperiam consequuntur deleniti obcaecati temporibus tenetur. Animi aspernatur autem, consectetur consequatur consequuntur doloremque dolores ducimus est et expedita explicabo fugit ipsum iure laboriosam laborum nihil nobis non officia quaerat reiciendis tempore, temporibus ut. Ad aliquam beatae cum delectus eius error eveniet excepturi explicabo illum in incidunt ipsum iste laboriosam, maxime non numquam placeat possimus quia quibusdam quis reprehenderit saepe similique soluta sunt ullam veniam voluptas voluptate. Aliquid aspernatur, consectetur deserunt iusto molestiae mollitia nesciunt odit perferendis provident rerum.',
    author: 'someBody',
    comments: [
      {
        id: Utils.randomID,
        author: 'Julius',
        body: 'Dictum! Factum!',
        commentedAt: new Date()
      },
      {
        id: Utils.randomID,
        author: 'Cicero',
        body: 'Veni! Vidi! Vici!',
        commentedAt: new Date()
      },
    ]
  },
  {
    id: Utils.randomID,
    title: 'Heading 3',
    img:   'https://i.picsum.photos/id/1023/200/200.jpg?hmac=MtNMS39i8o8sE6PiXNwABDxNtK4niBxaZWoX5KY3cyg',
    postedAt: new Date(),
    body: 'Aperiam asperiores aspernatur autem beatae blanditiis illo mollitia, nam nemo nihil perferendis quas repellendus sit suscipit vitae voluptas. Assumenda consectetur id illo laboriosam neque quasi reiciendis! Ab asperiores atque blanditiis consequatur culpa debitis doloribus enim excepturi facilis in incidunt inventore itaque laboriosam, minima minus necessitatibus odit officia officiis perspiciatis recusandae reprehenderit rerum tempora ullam! Corporis eum laudantium quaerat quo saepe! Autem commodi consequuntur cum eligendi eos error, esse eum magni maiores, maxime minus mollitia nemo nesciunt obcaecati odit perferendis quod quos repellat, sapiente sunt suscipit totam ullam vitae! Aspernatur, dolorum iste pariatur quasi recusandae similique sit tempora voluptatibus. Ad animi architecto commodi consectetur, consequuntur cupiditate, dolor eaque eos error eveniet facere impedit minima mollitia neque nisi numquam obcaecati odio odit officia qui quisquam quo sequi tempore vel veritatis. Ab amet at atque deleniti dicta dolor, doloremque earum esse excepturi inventore iure libero magni non odio placeat possimus qui quidem quod sed ullam ut veniam voluptas voluptatibus?',
    author: 'someBody',
    comments: [
      {
        id: Utils.randomID,
        author: 'Julius',
        body: 'Dictum! Factum!',
        commentedAt: new Date()
      },
      {
        id: Utils.randomID,
        author: 'Cicero',
        body: 'Veni! Vidi! Vici!',
        commentedAt: new Date()
      },
    ]
  }
];


