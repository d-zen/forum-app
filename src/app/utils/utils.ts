export default class Utils {
  static get randomID(): string {
    return String(Math.random().toString(36).substr(2, 9));
  }
}
